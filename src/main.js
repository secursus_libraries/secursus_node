const axios = require('axios');
const https = require('https');
const isObject = require('isobject');
const isString = require('is-string');

let BASE_URL = 'https://developer.secursus.com/api/';
const ERROR_URI_FIELD_FORMAT = 'Your Api URI must be an string.';
const ERROR_PARAMS_FIELD_FORMAT = 'Your Api parameters must be an Object.';
const ERROR_METHOD_FIELD_FORMAT = 'Only methods allowed is "GET", "POST" or "DELETE" for the Api request.';
const ERROR_ID_FIELD_FORMAT = 'Your parcels id must be a string.';
const ERROR_DATA_FIELD_FORMAT = 'Your arguments data must be an Object.';
const ERROR_AMOUNT_FIELD_FORMAT = 'Your parcels value must be a integer.';
const ERROR_UNKNOW_API_ID_FIELD_FORMAT = 'The Api ID is required.';
const ERROR_UNKNOW_API_KEY_FIELD_FORMAT = 'The Api Key is required.';

/**
 * @type {string}
 */
let apiId;

/**
 * @type {string}
 */
let apiKey;

/**
 * @type {[]}
 */
const availableMethod = ['GET', 'POST', 'DELETE'];

/**
 * Api constructor
 *
 * @param {string} apiIdInput
 * @param {string} apiKeyInput
 * @constructor
 */
function Api(apiIdInput, apiKeyInput, customEndpoint) {
    if (apiIdInput === '' || apiIdInput === null || !isString(apiIdInput)) {
        exception(ERROR_UNKNOW_API_ID_FIELD_FORMAT);
    }

    if (apiKeyInput === '' || apiKeyInput === null || !isString(apiKeyInput)) {
        exception(ERROR_UNKNOW_API_KEY_FIELD_FORMAT);
    }

    if (customEndpoint !== undefined) {
        BASE_URL = customEndpoint
    }

    apiId = apiIdInput;
    apiKey = apiKeyInput;

    checkAuth();
}

/**
 * Cancel Parcel Order
 *
 * @param {string} idParcel
 * @param {function} callback
 */
Api.prototype.cancelParcelOrder = function cancelParcelOrder(idParcel, callback) {
    if (!isString(idParcel)) {
        exception(ERROR_ID_FIELD_FORMAT);
    }

    request('parcels/' + idParcel + '/delete', {}, 'DELETE', function(data) {
        callback(data);
    });
}

/**
 * Create Parcel Order
 *
 * @param {Object} params
 * @param {function} callback
 */
Api.prototype.createParcelOrder = function createParcelOrder(params, callback) {
    if (!isObject(params)) {
        exception(ERROR_DATA_FIELD_FORMAT);
    }

    request('parcels/new', params, 'POST', function(data) {
        callback(data);
    });
}

/**
 * Get Insurance Amount
 *
 * @param {int} amount
 * @param {function} callback
 */
Api.prototype.getInsuranceAmount = function getInsuranceAmount(amount, callback) {
    if (!Number.isInteger(amount)) {
        exception(ERROR_AMOUNT_FIELD_FORMAT);
    }

    request('parcels/price', { 'parcel_value': amount }, 'POST', function(data) {
        callback(data);
    });
}

/**
 * Retrieve Parcel Order
 *
 * @param {string} idParcel
 * @param {function} callback
 */
Api.prototype.retrieveParcelOrder = function retrieveParcelOrder(idParcel, callback) {
    if (!isString(idParcel)) {
        exception(ERROR_ID_FIELD_FORMAT);
    }

    request('parcels/' + idParcel, {}, 'GET', function(data) {
        callback(data);
    });
}

/**
 * Retrieve Current Report
 *
 * @param {function} callback
 * @param {int} limit
 * @param {int} start
 * @param {string} orderField
 * @param {string} orderDir
 * @param {string} searchField
 * @param {string} searchValue
 */
Api.prototype.retrieveCurrentReport = function retrieveCurrentReport(
    callback,
    limit,
    start,
    orderField,
    orderDir,
    searchField,
    searchValue
) {
    request(
        constructArgs('parcels', limit, start, orderField, orderDir, searchField, searchValue),
        {},
        'GET',
        function(data) {
            callback(data);
        }
    );
}

/**
 * Retrieve History Report
 *
 * @param {function} callback
 * @param {int} limit
 * @param {int} start
 * @param {string} orderField
 * @param {string} orderDir
 * @param {string} searchField
 * @param {string} searchValue
 */
Api.prototype.retrieveHistoryReport = function retrieveHistoryReport(
    callback,
    limit,
    start,
    orderField,
    orderDir,
    searchField,
    searchValue
) {
    request(
        constructArgs('parcels/all', limit, start, orderField, orderDir, searchField, searchValue),
        {},
        'GET',
        function(data) {
            callback(data);
        }
    );
}

/**
 * Check Authentification
 *
 * @return {string}
 */
Api.prototype.checkAuthentification = function checkAuthentification(callback) {
    checkAuth(function (data) {
        callback(data);
    });
}

/**
 * Send Request
 *
 * @return {string}
 */
Api.prototype.sendRequest = function sendRequest(uri, params = {}, method = 'GET', callback) {
    request(uri, params, method, callback);
}

/**
 * Check Authentification
 *
 * @return {string}
 */
function checkAuth(callback) {
    request('auth', {}, 'GET', function (data) {
        if (callback) {
            callback(data);
        }
    });
};

/**
 * Construct Query Argument
 *
 * @return {string}
 */
function constructArgs(url, limit, start, orderField, orderDir, searchField, searchValue) {
    const args = {};

    if (limit !== undefined) { args.limit = limit; }
    if (start !== undefined) { args.start = start; }
    if (searchField !== undefined) { args.search_field = searchField; }
    if (searchValue !== undefined) { args.search_value = searchValue; }
    if (orderField !== undefined) { args.order_field = orderField; }
    if (orderDir !== undefined) { args.order_dir = orderDir; }

    const arrayArgs = [];
    Object.entries(args).forEach(function ([key, value]) {
        arrayArgs.push(key + '=' + value);
    });

    if (arrayArgs.length) {
        url = url + '?' + arrayArgs.join('&');
    }

    return url;
};

/**
 * Send request to Api
 *
 * @param {string} uri
 * @param {Object} params
 * @param {string} method
 * @param {function} callback
 */
function request(uri, params = {}, method = 'GET', callback) {
    if (uri === '' || !isString(uri)) {
        exception(ERROR_URI_FIELD_FORMAT);
    };

    if (!isObject(params)) {
        exception(ERROR_PARAMS_FIELD_FORMAT);
    }

    if (method === '' || !isString(method) || availableMethod.indexOf(method) === -1) {
        exception(ERROR_METHOD_FIELD_FORMAT);
    };

    var httpConfig = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: method,
        url: BASE_URL + uri,
        auth: {
            username: apiId,
            password: apiKey
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
    };

    if (method === 'GET') {
        httpConfig.params = params;
    } else if (method === 'POST') {
        httpConfig.data = params;
    }

    axios(httpConfig).then(function(response) {
        if (response.data) {
            callback(response.data);
        }
    }).catch(function (error) {
        if (error) {
            if (typeof error.response !== 'undefined') {
                exception(error.response.data);
                callback(error.response.data);
            } else {
                exception(error)
            }
        }
    });
}

/**
 * Exception
 *
 * @param {string} message
 * @return {void}
 */
function exception(message) {
    console.error(message);
}

module.exports = Api;
